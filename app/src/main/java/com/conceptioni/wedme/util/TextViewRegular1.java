package com.conceptioni.wedme.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


@SuppressLint("AppCompatCustomView")
public class TextViewRegular1 extends TextView {
    public TextViewRegular1(Context context) {
        super(context);
        init();
    }

    public TextViewRegular1(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewRegular1(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/ITCEDSCR_0.TTF");
        setTypeface(tf);
    }
}
