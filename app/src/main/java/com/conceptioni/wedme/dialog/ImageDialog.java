package com.conceptioni.wedme.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.conceptioni.wedme.R;
import com.makeramen.roundedimageview.RoundedImageView;

public class ImageDialog {
    Context context;
    Dialog dialog;
    ProgressBar progress;
    ImageView ivPrevious,ivNext;
    RoundedImageView imageView1;
    public ImageDialog(Context context){
        this.context=context;
    }

    public void ShowTermsDialog() {
        dialog=new Dialog(context);
        dialog.setContentView(R.layout.trems_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress=dialog.findViewById(R.id.progress);
        ivPrevious = dialog.findViewById(R.id.ivPrevious);
        ivNext = dialog.findViewById(R.id.ivNext);
        imageView1 = dialog.findViewById(R.id.imageView1);

        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("+++++click","");
                imageView1.setImageResource(R.drawable.small_image);
            }
        });

        ivPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("+++++click","pre");
                imageView1.setImageResource(R.drawable.friend_image);
            }
        });

        dialog.show();
    }

}
