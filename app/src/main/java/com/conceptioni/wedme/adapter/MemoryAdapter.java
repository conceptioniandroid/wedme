package com.conceptioni.wedme.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.conceptioni.wedme.R;
import com.conceptioni.wedme.activity.MemoryActivity;
import com.conceptioni.wedme.dialog.ImageDialog;
import com.makeramen.roundedimageview.RoundedImageView;

public class MemoryAdapter extends RecyclerView.Adapter<MemoryAdapter.Holder> {
    Context context;
    LayoutInflater layoutInflater;
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_item_memory,viewGroup,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
            holder.imageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new ImageDialog(context).ShowTermsDialog();
                }
            });
    }

    @Override
    public int getItemCount() {
        return 12;
    }

    public class Holder extends RecyclerView.ViewHolder {
        RoundedImageView imageView1;
        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView1 = itemView.findViewById(R.id.imageView1);
        }
    }
}
