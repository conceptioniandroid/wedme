package com.conceptioni.wedme.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.conceptioni.wedme.R;
import com.conceptioni.wedme.adapter.EventAdapter;

public class EventActivity extends AppCompatActivity {
    EventAdapter eventAdapter;
    RecyclerView rvEvent;
    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        init();
    }

    private void init() {
        rvEvent = findViewById(R.id.rvEvent);
        linearLayoutManager = new LinearLayoutManager(EventActivity.this);
        rvEvent.setLayoutManager(linearLayoutManager);
        eventAdapter = new EventAdapter();
        rvEvent.setAdapter(eventAdapter);
    }
}
