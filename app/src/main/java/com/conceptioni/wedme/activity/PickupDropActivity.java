package com.conceptioni.wedme.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;

import com.conceptioni.wedme.R;
import com.conceptioni.wedme.adapter.CustomSpinnerAdapter;
import com.conceptioni.wedme.util.TextViewRegular;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PickupDropActivity extends AppCompatActivity {

    Spinner spinner;
    TextViewRegular pickup, drop;
    String startDate;
    int BirthYear;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickupdrop);

        initialize();
        setSpinner();
        clicks();

    }

    private void initialize() {
        spinner = findViewById(R.id.spinner);
        pickup = findViewById(R.id.pickup);
        drop = findViewById(R.id.drop);
    }

    private void setSpinner() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("No. of guest");
        for (int i = 1; i < 11; i++) {
            arrayList.add(i + "");
        }

        spinner.setAdapter(new CustomSpinnerAdapter(this, R.layout.spinner_layout1, arrayList));
    }

    private void clicks() {
        pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickup.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg_withoutstroke));
                drop.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg_stroke));
                pickup.setTextColor(getResources().getColor(R.color.white));
                drop.setTextColor(getResources().getColor(R.color.colorPrimary));

            }
        });

        drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drop.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg_withoutstroke1));
                pickup.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg_stroke1));
                drop.setTextColor(getResources().getColor(R.color.white));
                pickup.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });
    }


}
