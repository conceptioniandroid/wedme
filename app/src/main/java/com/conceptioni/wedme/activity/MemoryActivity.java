package com.conceptioni.wedme.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.conceptioni.wedme.R;
import com.conceptioni.wedme.adapter.MemoryAdapter;
import com.conceptioni.wedme.util.ItemOffsetDecoration;

public class MemoryActivity extends AppCompatActivity {
    RecyclerView rvMemory;
    GridLayoutManager gridLayoutManager;
    MemoryAdapter memoryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory);
        init();
    }

    private void init() {
        rvMemory = findViewById(R.id.rvMemory);
        gridLayoutManager = new GridLayoutManager(MemoryActivity.this,3);
        rvMemory.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(MemoryActivity.this, R.dimen.item_offset);
        rvMemory.addItemDecoration(itemDecoration);

        memoryAdapter = new MemoryAdapter();
        rvMemory.setAdapter(memoryAdapter);
    }


}
