package com.conceptioni.wedme.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.conceptioni.wedme.R;
import com.conceptioni.wedme.adapter.BestFriendsAdapter;
import com.conceptioni.wedme.adapter.MemoryAdapter;
import com.conceptioni.wedme.util.ItemOffsetDecoration;

public class BestFriendsActivity extends AppCompatActivity {
    RecyclerView rvfriends;
    BestFriendsAdapter bestFriendsAdapter;
    GridLayoutManager gridLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_friends);
        init();
    }

    private void init() {
        rvfriends = findViewById(R.id.rvfriends);
        gridLayoutManager = new GridLayoutManager(BestFriendsActivity.this,2);
        rvfriends.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(BestFriendsActivity.this, R.dimen.dp_6);
        rvfriends.addItemDecoration(itemDecoration);

        bestFriendsAdapter = new BestFriendsAdapter();
        rvfriends.setAdapter(bestFriendsAdapter);
    }
}
