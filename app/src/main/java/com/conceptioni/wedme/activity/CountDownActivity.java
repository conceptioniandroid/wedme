package com.conceptioni.wedme.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.conceptioni.wedme.R;
import com.conceptioni.wedme.util.TextViewRegular;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CountDownActivity extends AppCompatActivity {

    Handler handler;
    Runnable runnable;
    CircularProgressBar pbday, pbhour, pbminute, pbsecond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_down);

        initialize();

        countDownStart();

    }

    private void initialize() {
        pbday = findViewById(R.id.pbdays);
        pbhour = findViewById(R.id.pbhours);
        pbminute = findViewById(R.id.pbminutes);
        pbsecond = findViewById(R.id.pbseconds);

        findViewById(R.id.ivhome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CountDownActivity.this, HomeActivity.class));
            }
        });
    }

    public void countDownStart() {
        handler = new Handler();
        runnable = new Runnable() {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void run() {
                handler.postDelayed(this, 1000);
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd", Locale.getDefault());
// Please here set your event date//YYYY-MM-DD
                    Date futureDate = dateFormat.parse("2018-12-25");
                    Date currentDate = new Date();
                    if (!currentDate.after(futureDate)) {
                        long diff = futureDate.getTime()
                                - currentDate.getTime();
                        long days = diff / (24 * 60 * 60 * 1000);
                        diff -= days * (24 * 60 * 60 * 1000);
                        long hours = diff / (60 * 60 * 1000);
                        diff -= hours * (60 * 60 * 1000);
                        long minutes = diff / (60 * 1000);
                        diff -= minutes * (60 * 1000);
                        long seconds = diff / 1000;
                        ((TextViewRegular) findViewById(R.id.days)).setText("" + String.format("%02d", days));
                        ((TextViewRegular) findViewById(R.id.hour)).setText("" + String.format("%02d", hours));
                        ((TextViewRegular) findViewById(R.id.minute)).setText("" + String.format("%02d", minutes));
                        ((TextViewRegular) findViewById(R.id.second)).setText("" + String.format("%02d", seconds));

                        pbday.setProgress(days);
                        pbhour.setProgress(hours);
                        pbminute.setProgress(minutes);
                        pbsecond.setProgress(seconds);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 1 * 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownStart();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}
