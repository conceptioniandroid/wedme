package com.conceptioni.wedme.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.conceptioni.wedme.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        clicks();

    }

    private void clicks() {

        findViewById(R.id.linareyoucoming).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, AreyoucomingActivity.class));
            }
        });

        findViewById(R.id.linpickup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, PickupDropActivity.class));
            }
        });

        findViewById(R.id.linwishes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SendWishesActivity.class));
            }
        });

        findViewById(R.id.llEvent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, EventActivity.class));
            }
        });

        findViewById(R.id.llKnowCouple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,KnowTheCoupleActivity.class));
            }
        });

        findViewById(R.id.llRate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,RatingActivity.class));
            }
        });

        findViewById(R.id.llMemory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,MemoryActivity.class));
            }
        });
        findViewById(R.id.llGifts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,GiftsActivity.class));
            }
        });

        findViewById(R.id.llfriends).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,BestFriendsActivity.class));
            }
        });
    }
}
