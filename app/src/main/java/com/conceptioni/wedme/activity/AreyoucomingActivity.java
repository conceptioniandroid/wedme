package com.conceptioni.wedme.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Spinner;

import com.conceptioni.wedme.R;
import com.conceptioni.wedme.adapter.CustomSpinnerAdapter;

import java.util.ArrayList;

public class AreyoucomingActivity extends AppCompatActivity {

    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_areyoucoming);

        spinner = findViewById(R.id.spinner);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("No. of guest");
        for (int i = 1; i < 11; i++) {
            arrayList.add(i + "");
        }

        spinner.setAdapter(new CustomSpinnerAdapter(this, R.layout.spinner_layout1, arrayList));

    }
}
